---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: bigbang-app
  namespace: ${argocd_ns}
operation:
  initiatedBy:
    automated: true
  retry:
    backoff:
      duration: 20s
      factor: 2
      maxDuration: 3m0s
    limit: 2
  sync:
    prune: true
    revision: f60369d9836c41abe6f45825ba5af99afd8c286f
    syncOptions:
    - CreateNamespace=true
spec:
  destination:
    namespace: ${argocd_ns}
    server: https://kubernetes.default.svc
  project: default
  source:
    directory:
      jsonnet:
        extVars:
        - name: ''
          value: ''
      recurse: true
    path: manifests
    repoURL: ${argocd_bigbang_repo}
    targetRevision: HEAD
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
    retry:
      backoff:
        duration: 20s
        factor: 2
        maxDuration: 3m0s
      limit: 2
    syncOptions:
    - CreateNamespace=true