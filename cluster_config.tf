# resource "local_file" "test_local_template" {
#     content  = yamldecode(templatefile("${path.module}/k8s_manifests/argocd.yml.tpl",{argocd_ns = "test_argocd_ns"}))
#     filename = "${path.module}/argocd.yml"
# }

resource "null_resource" "apply_base_k8s_CNI_plugin" {
  triggers = {
    instance = google_compute_instance.control_plane_instance[0].id
  }

  depends_on = [
    null_resource.start_one_node_cluster,
    ]

  connection {
    type        = "ssh"
    user        = var.gcp_user
    private_key = tls_private_key.instance_tls.private_key_pem
    host        = google_compute_instance.control_plane_instance[0].network_interface[0].access_config[0].nat_ip
  }

  # Install Helm
  provisioner "remote-exec" {
    inline = [
      "echo '*************' ${local.output_logging}",
      "echo 'RUNNING: Install Helm' ${local.output_logging}",
      "echo '*************' ${local.output_logging}",
      "curl -L https://get.helm.sh/helm-v3.8.1-linux-amd64.tar.gz -o helm-linux-amd64.tar.gz ${local.output_logging}",
      "tar -zxf helm-linux-amd64.tar.gz ${local.output_logging}",
      "sudo cp -v linux-amd64/helm /usr/local/bin/ ${local.output_logging}",
      "rm -v helm-linux-amd64.tar.gz ${local.output_logging}",
      "rm -vrf linux-amd64 ${local.output_logging}",
      "echo '*************' ${local.output_logging}",
      "echo 'FINISHED: Install Helm' ${local.output_logging}",
      "echo '*************' ${local.output_logging}",
    ]
  }

  # Install CNI (Cilium)
  provisioner "remote-exec" {
    inline = [<<-EOT
      echo '*************' ${local.output_logging}
      echo 'RUNNING: Install CNI (Cilium)' ${local.output_logging}
      echo $(date) ${local.output_logging}
      echo '*************' ${local.output_logging}
      helm repo add cilium https://helm.cilium.io/ ${local.output_logging}
      helm install cilium cilium/cilium --version 1.11.2 --namespace kube-system ${local.output_logging}
      kubectl get pods --all-namespaces -o custom-columns=NAMESPACE:.metadata.namespace,NAME:.metadata.name,HOSTNETWORK:.spec.hostNetwork --no-headers=true | grep '<none>' | awk '{print "-n "$1" "$2}' | xargs -L 1 -r kubectl delete pod ${local.output_logging}
      curl -L --remote-name-all https://github.com/cilium/cilium-cli/releases/latest/download/cilium-linux-amd64.tar.gz{,.sha256sum} ${local.output_logging}
      sha256sum --check cilium-linux-amd64.tar.gz.sha256sum ${local.output_logging}
      sudo tar xzvfC cilium-linux-amd64.tar.gz /usr/local/bin ${local.output_logging}
      rm cilium-linux-amd64.tar.gz{,.sha256sum} ${local.output_logging}
      sudo rm -vf /usr/local/bin/helm ${local.output_logging}
      #cilium status --wait ${local.output_logging}
      #cilium connectivity test ${local.output_logging}
      echo '*************' ${local.output_logging}
      echo 'FINISHED: Install CNI (Cilium)' ${local.output_logging}
      echo '*************' ${local.output_logging}
      EOT
    ]
  }
}

resource "null_resource" "setup_k8s_gitops" {
  triggers = {
    instance = google_compute_instance.control_plane_instance[0].id
  }

  depends_on = [
    null_resource.start_one_node_cluster,
    null_resource.apply_base_k8s_CNI_plugin
    ]

  connection {
    type        = "ssh"
    user        = var.gcp_user
    private_key = tls_private_key.instance_tls.private_key_pem
    host        = google_compute_instance.control_plane_instance[0].network_interface[0].access_config[0].nat_ip
  }

  # Install ArgoCD
  provisioner "remote-exec" {
    inline = [
      "echo '*************' ${local.output_logging}",
      "echo 'RUNNING: Install ArgoCD' ${local.output_logging}",
      "echo $(date) ${local.output_logging}",
      "echo '*************' ${local.output_logging}",
      #"cat < ${local.argo_configuration_yaml} | kubectl apply -f - ${local.output_logging}",
      "kubectl create namespace argocd ${local.output_logging}",
      "kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml ${local.output_logging}",
      "kubectl rollout status deployment argocd-server --watch=true -n ${var.argocd_ns} ${local.output_logging}",
      "echo '*************' ${local.output_logging}",
      "echo 'FINISHED: Install ArgoCD' ${local.output_logging}",
      "echo '*************' ${local.output_logging}",
    ]
  }

  # Set ArgoCD admin password
  provisioner "remote-exec" {
    inline = [
      "echo '*************' ${local.output_logging}",
      "echo 'RUNNING: Set ArgoCD admin password' ${local.output_logging}",
      "echo $(date) ${local.output_logging}",
      "echo '*************' ${local.output_logging}",
      "curl -L https://github.com/argoproj/argo-cd/releases/download/v2.3.1/argocd-linux-amd64 -o argocd ${local.output_logging}",
      "chmod +x argocd ${local.output_logging}",
      "export ARGOCD_ADMIN_PASS=$(kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath=\"{.data.password}\" | base64 -d; echo ${local.output_logging})",
      "kubectl port-forward svc/argocd-server -n ${var.argocd_ns} 8080:443 2>&1 >/dev/null &",
      "sleep 5",
      "ss -ant | grep LISTEN | grep 8080 ${local.output_logging}",
      "./argocd login 127.0.0.1:8080 --username admin --password $ARGOCD_ADMIN_PASS --insecure ${local.output_logging}",
      "./argocd account update-password --current-password $ARGOCD_ADMIN_PASS --new-password ${nonsensitive(var.argocd_admin_pass)} --insecure ${local.output_logging}",
      "rm -vf argocd ${local.output_logging}",
      "echo '*************' ${local.output_logging}",
      "echo 'FINISHED: Set ArgoCD admin password' ${local.output_logging}",
      "echo '*************' ${local.output_logging}",
    ]
  }  
}

resource "null_resource" "configure_gitops_manifests" {
  triggers = {
    instance = null_resource.setup_k8s_gitops.id
  }

  depends_on = [
    null_resource.setup_k8s_gitops,
    ]

  connection {
    type        = "ssh"
    user        = var.gcp_user
    private_key = tls_private_key.instance_tls.private_key_pem
    host        = google_compute_instance.control_plane_instance[0].network_interface[0].access_config[0].nat_ip
  }
  
  # Configure cluster manifests
  provisioner "remote-exec" {
    inline = [
      "echo '*************' ${local.output_logging}",
      "echo 'RUNNING: Configure cluster manifests' ${local.output_logging}",
      "echo $(date) ${local.output_logging}",
      "echo '*************' ${local.output_logging}",
      "echo \"${local.argo_bigbang_yaml}\" ${local.output_logging}", 
      "echo \"${local.argo_bigbang_yaml}\" ${local.output_logging} | kubectl apply -f -",
      "echo '*************' ${local.output_logging}",
      "echo 'FINISHED: Configure cluster manifests' ${local.output_logging}",
      "echo '*************' ${local.output_logging}"
    ]
  }
}