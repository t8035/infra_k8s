resource "google_compute_network" "k8s_network" {
  name                    = "vpc-${var.k8s_cluster_id}"
  auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "k8s_subnet" {
  name          = "k8s-subnet-${var.gcp_k8s_region}"
  ip_cidr_range = var.gcp_k8s_network_cidr
  region        = var.gcp_k8s_region
  network       = google_compute_network.k8s_network.id
}

resource "google_dns_managed_zone" "k8s_private_domain" {
  name        = "zone-${var.k8s_cluster_id}"
  dns_name    = local.cluster_domain
  description = "Kubernetes cluster ${var.k8s_cluster_id} domain"

  visibility = "private"

  private_visibility_config {
    networks {
      network_url = google_compute_network.k8s_network.id
    }
  }
}

resource "google_dns_record_set" "api_dns_entry" {
  name = "${var.k8s_dns_api_name}.${local.cluster_domain}"
  type = "CNAME"

  managed_zone = google_dns_managed_zone.k8s_private_domain.name

  rrdatas = ["${google_compute_instance.control_plane_instance[0].hostname}."]
}

resource "google_dns_record_set" "controller_dns_entry" {
  count = var.k8s_control_plane_size

  name = "${google_compute_instance.control_plane_instance[count.index].hostname}."
  type = "A"
  ttl  = 300

  managed_zone = google_dns_managed_zone.k8s_private_domain.name

  rrdatas = [google_compute_instance.control_plane_instance[count.index].network_interface[0].network_ip]
}

resource "google_dns_record_set" "data_plane_dns_entry" {
  count = var.k8s_data_plane_size

  name = "${google_compute_instance.data_plane_instance[count.index].hostname}."
  type = "A"
  ttl  = 300

  managed_zone = google_dns_managed_zone.k8s_private_domain.name

  rrdatas = [google_compute_instance.data_plane_instance[count.index].network_interface[0].network_ip]
}

resource "google_compute_firewall" "k8s_firewall_internal" {
  name    = "${var.k8s_cluster_id}-allow-internal"
  network = google_compute_network.k8s_network.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["0-65535"]
  }

  allow {
    protocol = "udp"
    ports    = ["0-65535"]
  }

  source_ranges = [var.gcp_k8s_network_cidr]
}

resource "google_compute_firewall" "k8s_firewall_ssh" {
  name    = "${var.k8s_cluster_id}-allow-ssh"
  network = google_compute_network.k8s_network.name

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "k8s_firewall_api" {
  name    = "${var.k8s_cluster_id}-allow-k8s-api"
  network = google_compute_network.k8s_network.name

  allow {
    protocol = "tcp"
    ports    = ["6443"]
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "k8s_firewall_https" {
  name    = "${var.k8s_cluster_id}-allow-https"
  network = google_compute_network.k8s_network.name

  allow {
    protocol = "tcp"
    ports    = ["443"]
  }

  source_ranges = ["0.0.0.0/0"]
}