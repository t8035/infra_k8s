resource "google_compute_instance" "data_plane_instance" {
  count = var.k8s_data_plane_size

  name         = "${var.k8s_cluster_id}-node-${count.index}"
  hostname     = "${var.k8s_cluster_id}-node-${count.index}.${var.k8s_domain}"
  machine_type = var.gce_node_instance_type
  zone         = var.gcp_k8s_zones[count.index % 3]
  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = var.gce_instance_image
    }
  }

  network_interface {
    network = google_compute_network.k8s_network.name
    subnetwork = google_compute_subnetwork.k8s_subnet.name
    access_config {
    }
  }

  description = "Kubernetes data plane machine"
  labels = {
    name    = "${var.k8s_cluster_id}-node-${count.index}"
    service = "kubernetes"
    app     = "kubernetes"
    layer   = "data-plane"
    index   = "${count.index}"
    project = "tfm"
  }

  metadata = {
    ssh-keys = "${var.gcp_user}:${tls_private_key.instance_tls.public_key_openssh}"
  }
  connection {
    type        = "ssh"
    user        = var.gcp_user
    private_key = tls_private_key.instance_tls.private_key_pem
    host        = self.network_interface[0].access_config[0].nat_ip
  }

  # Wait for instance readiness
  provisioner "remote-exec" {
    inline = [
      "cat /etc/os-release"
    ]
  }

}

resource "null_resource" "configure_node_instance" {
  count = var.k8s_data_plane_size

  triggers = {
    instance = google_compute_instance.data_plane_instance[count.index].id
    k8s_base = local.k8s_base_version
  }

  connection {
    type        = "ssh"
    user        = var.gcp_user
    private_key = tls_private_key.instance_tls.private_key_pem
    host        = google_compute_instance.data_plane_instance[count.index].network_interface[0].access_config[0].nat_ip
  }

  # Add K8s yum repository
  provisioner "remote-exec" {
    inline = [<<-EOT
        echo '*************'
        echo 'RUNNING: Add K8s yum repository'
        echo '*************'
        cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
        [kubernetes]
        name=Kubernetes
        baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
        enabled=1
        gpgcheck=1
        repo_gpgcheck=1
        gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
        exclude=kubelet kubeadm kubectl
        EOF
        echo '*************'
        echo 'FINISHED: Add K8s yum repository'
        echo '*************'
        EOT
    ]
  }

  # Disable local security (just for testing purposes)
  provisioner "remote-exec" {
    inline = [
      "echo '*************'",
      "echo 'RUNNING: Disable local security (just for testing purposes)'",
      "echo '*************'",
      "sudo setenforce 0",
      "sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config",
      "sudo systemctl disable firewalld --now",
      "echo '*************'",
      "echo 'FINISHED: Disable local security (just for testing purposes)'",
      "echo '*************'"
    ]
  }

  # Cri-O system dependencies configuration
  provisioner "remote-exec" {
    inline = [<<-EOT
        echo '*************'
        echo 'RUNNING: Cri-O system dependencies configuration'
        echo '*************'
        cat <<EOF | sudo tee /etc/modules-load.d/crio.conf
        overlay
        br_netfilter
        EOF

        sudo modprobe overlay
        sudo modprobe br_netfilter

        cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
        net.bridge.bridge-nf-call-iptables  = 1
        net.ipv4.ip_forward                 = 1
        net.bridge.bridge-nf-call-ip6tables = 1
        EOF
        sudo sysctl --system
        echo '*************'
        echo 'FINISHED: Cri-O system dependencies configuration'
        echo '*************'
        EOT
    ]
  }

  # Install package dependencies
  provisioner "remote-exec" {
    inline = [
      "echo '*************'",
      "echo 'RUNNING: Install package dependencies'",
      "echo '*************'",
      "sudo yum install -y iproute-tc",
      "echo '*************'",
      "echo 'FINISHED: Install package dependencies'",
      "echo '*************'"
    ]
  }

  # Install Cri-O as container engine
  provisioner "remote-exec" {
    inline = [
      "echo '*************'",
      "echo 'RUNNING: Install Cri-O as container engine'",
      "echo '*************'",
      "sudo curl -L -o /etc/yum.repos.d/devel:kubic:libcontainers:stable.repo https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/${local.base_OS}/devel:kubic:libcontainers:stable.repo",
      "sudo curl -L -o /etc/yum.repos.d/devel:kubic:libcontainers:stable:cri-o:${local.k8s_base_version}.repo https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable:/cri-o:/${local.k8s_base_version}/${local.base_OS}/devel:kubic:libcontainers:stable:cri-o:1.23.repo",
      "sudo yum install -y cri-o",
      "sudo systemctl daemon-reload",
      "sudo systemctl enable crio --now",
      "sudo systemctl status crio --no-pager",
      "echo '*************'",
      "echo 'FINISHED: Install Cri-O as container engine'",
      "echo '*************'"
    ]
  }

  # Install K8s tools
  provisioner "remote-exec" {
    inline = [
      "echo '*************'",
      "echo 'RUNNING: Install K8s tools'",
      "echo '*************'",
      "sudo yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes",
      "sudo systemctl enable --now kubelet",
      "echo 'Installed kubectl version:'",
      "kubectl version --client --output=yaml",
      "echo 'Installed kubeadm version:'",
      "kubeadm version -o yaml",
      "echo '*************'",
      "echo 'FINISHED: Install K8s tools'",
      "echo '*************'"
    ]
  }

  # Configure SSH
  provisioner "remote-exec" {
    inline = [ <<-EOT
      echo '*************'
      echo 'RUNNING: Configure SSH'
      echo '*************'
      cat << EOF > $HOME/.ssh/config
      Host *
        User ${var.gcp_user}
        IdentityFile ~/.ssh/${local.ssh_identity_file}
        IdentitiesOnly yes
        StrictHostKeyChecking no
        UserKnownHostsFile /dev/null
        ConnectionAttempts 3
        ControlMaster auto
        ControlPath ~/.ssh/control-%h-%p-%r
        ControlPersist yes
      EOF
      chmod 600 $HOME/.ssh/config
      echo '*************'
      echo 'FINISHED: Configure SSH'
      echo '*************'
      EOT
    ]
  }
  
}

resource "null_resource" "deploy_private_key_data_plane" {
  count = var.k8s_data_plane_size

  triggers = {
    instance = google_compute_instance.data_plane_instance[count.index].id
  }

  # render private key
  provisioner "local-exec" {
    command = "echo '${nonsensitive(tls_private_key.instance_tls.private_key_pem)}' > ${local.ssh_identity_file} && chmod 400 ${local.ssh_identity_file}"
  }
  provisioner "local-exec" {
    command = <<-EOT
      ssh -i ${local.ssh_identity_file} -o StrictHostKeyChecking=no ${var.gcp_user}@${google_compute_instance.data_plane_instance[count.index].network_interface[0].access_config[0].nat_ip} \
      -C mkdir .ssh
      scp -i ${local.ssh_identity_file} -o StrictHostKeyChecking=no ${local.ssh_identity_file} \
      ${var.gcp_user}@${google_compute_instance.data_plane_instance[count.index].network_interface[0].access_config[0].nat_ip}:.ssh/
      ssh -i ${local.ssh_identity_file} -o StrictHostKeyChecking=no ${var.gcp_user}@${google_compute_instance.data_plane_instance[count.index].network_interface[0].access_config[0].nat_ip} \
      -C chmod 600 .ssh/${local.ssh_identity_file}
    EOT
  }

  # remove private key
  provisioner "local-exec" {
    command = "rm -vf ${local.ssh_identity_file}"
  }
}