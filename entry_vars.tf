variable "gcp_default_project" {
  description = "Default project to use on GCP"
  type        = string
}

variable "gcp_user" {
  description = "Default user for GCP"
  type        = string
}

variable "gcp_default_region" {
  description = "Default region to deploy resources on GCP"
  type        = string
  default = "europe-west3"
}

variable "gcp_default_zone" {
  description = "Default zone to deploy resources"
  type        = string
  default = "europe-west3-a"
}

variable "gcp_k8s_region" {
  description = "Region to deploy k8s resources on GCP"
  type        = string
  default = "europe-west3"
}

variable "gcp_k8s_zones" {
  description = "Selected zones to deploy k8s"
  type        = list(string)
  default = ["europe-west3-a", "europe-west3-b", "europe-west3-c"]
}

variable gcp_k8s_network_cidr {
  description = "Default zone to deploy resources"
  type        = string
  default = "10.16.0.0/16"
}

variable "gce_controller_instance_type" {
  description = "Machine type for K8s controller"
  type        = string
  default = "e2-medium"
}

variable "gce_node_instance_type" {
  description = "Machine type for K8s nodes"
  type        = string
  default = "e2-medium"
}
variable "gce_instance_image" {
  description = "Controller machine base image"
  type        = string
  default = "projects/centos-cloud/global/images/centos-stream-8-v20220303"
}

variable "k8s_version" {
  description = "k8s version to use"
  type        = string
  default = "1.23.4"
}

variable "k8s_cluster_id" {
  description = "k8s cluster id"
  default     = "k8s-test-cluster"
  type        = string
}
variable "k8s_control_plane_size" {
  description = "number of instances for control plane"
  default     = 1
  type        = number
}

variable "k8s_data_plane_size" {
  description = "number of instances for control plane"
  default     = 0
  type        = number
}

variable "k8s_domain" {
  description = "K8s Domain"
  default     = "secure-kube.tfm"
  type        = string
}
variable "k8s_dns_api_name" {
  description = "K8s LB DNS name"
  default     = "k8s-api"
  type        = string
}