resource "null_resource" "fetch_kubeconfig" {
  triggers = {
    instance = google_compute_instance.control_plane_instance[0].id
  }

  depends_on = [
    null_resource.start_one_node_cluster
  ]

  # render private key
  provisioner "local-exec" {
    command = "echo '${nonsensitive(tls_private_key.instance_tls.private_key_pem)}' > ${local.ssh_identity_file} && chmod 400 chmod"
  }
  provisioner "local-exec" {
    command = <<-EOT
      scp -i ${local.ssh_identity_file} -o StrictHostKeyChecking=no \
      ${var.gcp_user}@${google_compute_instance.control_plane_instance[0].network_interface[0].access_config[0].nat_ip}:.kube/config ${path.module}/kubeconfig
    EOT
  }

  # remove private key
  provisioner "local-exec" {
    command = "rm -vf ${local.ssh_identity_file}"
  }
}
