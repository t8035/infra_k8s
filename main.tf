locals {
  output_logging = "2>&1 | sudo tee -a /root/tf_deployment.out"
  k8s_base_version = regex("^[[:digit:]]+.[[:digit:]]+", var.k8s_version)
  base_OS          = "CentOS_8_Stream"
  ssh_identity_file = "private_key_gcp.pem"
  cluster_domain = "${var.k8s_domain}."

  argo_configuration_yaml = templatefile("${path.module}/k8s_manifests/argocd.yml.tpl",{argocd_ns = "${var.argocd_ns}"})
  argo_bigbang_yaml = templatefile("${path.module}/k8s_manifests/argocd_base_app.yml.tpl",{argocd_ns = "${var.argocd_ns}", argocd_bigbang_repo = "${var.argocd_bigbang_repo}"})
}