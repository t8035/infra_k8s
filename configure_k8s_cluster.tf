resource "null_resource" "start_one_node_cluster" {

  triggers = {
    instance = google_compute_instance.control_plane_instance[0].id
  }

  depends_on = [
    google_dns_record_set.api_dns_entry,
    null_resource.deploy_private_key_controller[0],
    null_resource.configure_controller_instance[0]
    ]

  connection {
    type        = "ssh"
    user        = var.gcp_user
    private_key = tls_private_key.instance_tls.private_key_pem
    host        = google_compute_instance.control_plane_instance[0].network_interface[0].access_config[0].nat_ip
  }

  # Init a kubernetes control plane
  provisioner "remote-exec" {
    inline = [
      "echo '*************' ${local.output_logging}",
      "echo 'RUNNING: Init a kubernetes control plane' ${local.output_logging}",
      "echo $(date) ${local.output_logging}",
      "echo '*************' ${local.output_logging}",
      "sudo kubeadm init --control-plane-endpoint=${var.k8s_dns_api_name}.${var.k8s_domain} ${local.output_logging}",
      "mkdir -p $HOME/.kube ${local.output_logging}",
      "sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config ${local.output_logging}",
      "sudo chown $(id -u):$(id -g) $HOME/.kube/config ${local.output_logging}",
      "kubectl get nodes ${local.output_logging}",
      "kubectl get pods --namespace kube-system ${local.output_logging}",
      "kubectl taint nodes --all node-role.kubernetes.io/master- ${local.output_logging}",
      "echo '*************' ${local.output_logging}",
      "echo 'FINISHED: Init a kubernetes control plane' ${local.output_logging}",
      "echo '*************' ${local.output_logging}",
      "curl -k https://localhost:6443/livez?verbose"
    ]
  }
}

resource "null_resource" "join_control_plane" {
  count = var.k8s_control_plane_size - 1

  triggers = {
    instance = google_compute_instance.control_plane_instance[count.index + 1].id
  }

  depends_on = [
    null_resource.start_one_node_cluster,
    null_resource.configure_controller_instance,
    null_resource.deploy_private_key_controller
  ]

  connection {
    type        = "ssh"
    user        = var.gcp_user
    private_key = tls_private_key.instance_tls.private_key_pem
    host        = google_compute_instance.control_plane_instance[count.index + 1].network_interface[0].access_config[0].nat_ip
  }

  # Deploy cluster certificates
  provisioner "remote-exec" {
    inline = [ <<-EOT
      echo '*************' ${local.output_logging}
      echo 'RUNNING: Deploy cluster certificates' ${local.output_logging}
      echo $(date) ${local.output_logging}
      echo '*************' ${local.output_logging}
      export NODE_IP=${google_compute_instance.control_plane_instance[count.index + 1].network_interface[0].network_ip}
      sudo mkdir -p /etc/kubernetes/pki/etcd
      sudo chown -R root:root /etc/kubernetes
      sudo chmod -R 755 /etc/kubernetes
      for CERT_FILE in {"ca.crt","ca.key","sa.key","sa.pub","front-proxy-ca.crt","front-proxy-ca.key","etcd/ca.crt","etcd/ca.key"}; do
        echo "transfering $CERT_FILE ..." ${local.output_logging}
        export TMP_FILE=$(echo $CERT_FILE | sed "s@/@_@")-${count.index}
        ssh ${var.k8s_dns_api_name}.${var.k8s_domain} \
        -C "sudo cp -v /etc/kubernetes/pki/$CERT_FILE /home/${var.gcp_user}/$TMP_FILE && \
        sudo chown -v ${var.gcp_user}:${var.gcp_user} /home/${var.gcp_user}/$TMP_FILE && \
        scp -q /home/${var.gcp_user}/$TMP_FILE $NODE_IP:/home/${var.gcp_user}/$TMP_FILE && \
        rm -fv /home/${var.gcp_user}/$TMP_FILE" ${local.output_logging}
        sudo mv -v /home/${var.gcp_user}/$TMP_FILE /etc/kubernetes/pki/$CERT_FILE ${local.output_logging}
        sudo chown -v root:root /etc/kubernetes/pki/$CERT_FILE
      done
      sudo chmod -v 400 /etc/kubernetes/pki/*.key ${local.output_logging}
      sudo chmod -v 400 /etc/kubernetes/pki/etcd/*.key ${local.output_logging}
      echo '*************' ${local.output_logging}
      echo 'FINISHED: Deploy cluster certificates' ${local.output_logging}
      echo '*************' ${local.output_logging}
      EOT
    ]
  }

  # Join a kubernetes control plane
  provisioner "remote-exec" {
    inline = [<<-EOT
      echo '*************' ${local.output_logging}
      echo 'RUNNING: Join a kubernetes control plane' ${local.output_logging}
      echo $(date) ${local.output_logging}
      echo '*************' ${local.output_logging}
      export KUBE_CERT=$(ssh ${var.k8s_dns_api_name}.${var.k8s_domain} \
      -C openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 -hex | sed 's/^.* //' ${local.output_logging})
      export JOIN_COMMAND=$(ssh ${var.k8s_dns_api_name}.${var.k8s_domain} \
      -C kubeadm token create --ttl 10m --print-join-command ${local.output_logging})
      sudo $JOIN_COMMAND --control-plane ${local.output_logging}
      mkdir -p $HOME/.kube ${local.output_logging}
      sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config ${local.output_logging}
      sudo chown $(id -u):$(id -g) $HOME/.kube/config
      kubectl get nodes ${local.output_logging}
      kubectl get pods --namespace kube-system ${local.output_logging}
      kubectl taint nodes --all node-role.kubernetes.io/master- ${local.output_logging}
      curl -k https://localhost:6443/livez?verbose ${local.output_logging}
      echo '*************' ${local.output_logging}
      echo 'FINISHED: Join a kubernetes control plane' ${local.output_logging}
      echo '*************' ${local.output_logging}
      EOT
    ]
  }
}

resource "null_resource" "join_data_plane" {
  count = var.k8s_data_plane_size

  triggers = {
    instance = google_compute_instance.control_plane_instance[count.index].id
  }

  depends_on = [
    null_resource.start_one_node_cluster,
    null_resource.configure_controller_instance,
    null_resource.deploy_private_key_data_plane
  ]

  connection {
    type        = "ssh"
    user        = var.gcp_user
    private_key = tls_private_key.instance_tls.private_key_pem
    host        = google_compute_instance.data_plane_instance[count.index].network_interface[0].access_config[0].nat_ip
  }

  # Join a kubernetes control plane
  provisioner "remote-exec" {
    inline = [<<-EOT
      echo '*************' ${local.output_logging}
      echo 'RUNNING: Join a kubernetes cluster' ${local.output_logging}
      echo $(date) ${local.output_logging}
      echo '*************' ${local.output_logging}
      export JOIN_COMMAND=$(ssh -q ${var.k8s_dns_api_name}.${var.k8s_domain} \
      -C kubeadm token create --ttl 10m --print-join-command ${local.output_logging})
      sudo $JOIN_COMMAND
      echo '*************' ${local.output_logging}
      echo 'FINISHED: Join a kubernetes cluster' ${local.output_logging}
      echo '*************' ${local.output_logging}
      EOT
    ]
  }

}
