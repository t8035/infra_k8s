variable "argocd_ns" {
  description = "Namespace for ArgoCD"
  default     = "argocd"
  type        = string
}

variable "argocd_admin_pass" {
  description = "Admin password for ArgoCD"
  type        = string
  sensitive = true
}

variable "argocd_bigbang_repo" {
  description = "K8s App of apps repository"
  type        = string
  default = "https://gitlab.com/t8035/argocd_bigbang_app.git"
}