terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.11.0"
    }
    # kubernetes = {
    #   source = "hashicorp/kubernetes"
    #   version = "2.8.0"
    # }
  }
}

provider "google" {
  project = var.gcp_default_project
  region  = var.gcp_default_region
  zone    = var.gcp_default_zone
}

# provider "kubernetes" {
#   config_path = "${path.module}/kubeconfig"
# }