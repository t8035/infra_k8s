resource "tls_private_key" "instance_tls" {
  algorithm = "RSA"
}

output "private_key" {
  value = nonsensitive(tls_private_key.instance_tls.private_key_pem)
}

# resource "local_sensitive_file" "private_key" {
#   content  = tls_private_key.instance_tls.private_key_pem
#   filename = local.ssh_identity_file
# }
